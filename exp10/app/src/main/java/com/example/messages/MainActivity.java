package com.example.messages;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.telephony.SmsManager;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {

    EditText e1,e2;
    Button b;
    String phone,message;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        b = (Button) findViewById(R.id.button);
        e1 = (EditText) findViewById(R.id.editTextTextPersonName);
        e2 = (EditText) findViewById(R.id.editTextTextPersonName2);

        phone = e1.getText().toString().trim();
        message = e2.getText().toString().trim();

        b.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(!phone.equals("") && !message.equals("")){
                    SmsManager sms = SmsManager.getDefault();
                    Toast.makeText(getApplicationContext(),"Senting message to " + phone,Toast.LENGTH_LONG).show();
                    sms.sendTextMessage(phone,null,message,null,null);

                    Toast.makeText(getApplicationContext(),"Sent succsessfully",Toast.LENGTH_LONG).show();
                }
            }
        });



    }
}