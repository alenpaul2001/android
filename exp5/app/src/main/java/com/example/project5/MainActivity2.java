package com.example.project5;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.widget.EditText;
import android.widget.Toast;

public class MainActivity2 extends AppCompatActivity {
    String s;
    EditText e;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main2);
        Intent i=getIntent();
        s=i.getStringExtra("name");
        Toast.makeText(MainActivity2.this,s,Toast.LENGTH_LONG).show();
        e=(EditText) findViewById(R.id.edittext2);
        e.setText(s);
    }
}