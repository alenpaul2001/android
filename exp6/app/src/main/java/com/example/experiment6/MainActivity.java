package com.example.experiment6;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.widget.Button;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        Button linear_layout = findViewById(R.id.linear_layout);
        Button relative_layout  = findViewById(R.id.relative_layout);
        Button table_layout = findViewById(R.id.table_layout);
        Button absolute_layout = findViewById(R.id.absolute_layout);
        Button constrain_layout = findViewById(R.id.contraint_layout);

        linear_layout.setOnClickListener((v) -> {
            Intent i = new Intent(MainActivity.this, Main2Activity.class);
            startActivity(i);
        });

        relative_layout.setOnClickListener((v) -> {
            Intent i = new Intent(MainActivity.this, Main3Activity.class);
            startActivity(i);
        });

        table_layout.setOnClickListener((v) -> {
            Intent i = new Intent(MainActivity.this, Main5Activity.class);
            startActivity(i);
        });

        absolute_layout.setOnClickListener((v) -> {
            Intent i = new Intent(MainActivity.this, Main4Activity.class);
            startActivity(i);
        });

        constrain_layout.setOnClickListener((v) -> {
            Intent i = new Intent(MainActivity.this, Main6Activity.class);
            startActivity(i);
        });
    }
}
