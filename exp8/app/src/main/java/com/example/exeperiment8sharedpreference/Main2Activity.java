package com.example.exeperiment8sharedpreference;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

public class Main2Activity extends AppCompatActivity {
    EditText E1;
    String S;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main2);
        E1= (EditText) findViewById(R.id.editText2);
        Intent i=new Intent();
        S=i.getStringExtra("msg");
        Toast.makeText(Main2Activity.this,S,Toast.LENGTH_LONG);
        E1=(EditText)findViewById(R.id.editText2);
        E1.setText(S);


    }
}
