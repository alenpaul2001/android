package com.example.unknown;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.widget.EditText;
import android.content.SharedPreferences;
import android.widget.Toast;

public class MainActivity2 extends AppCompatActivity {

    EditText t;
    String s;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main2);

        setContentView(R.layout.activity_main2);
        SharedPreferences sp = getSharedPreferences("pref",MODE_PRIVATE);
        String dataPassed = sp.getString("msg", "No data available");
        t=(EditText) findViewById(R.id.editTextTextPersonName3);
        t.setText(dataPassed);
    }
}
