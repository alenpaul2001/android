package com.example.sharedpreference;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.nio.charset.StandardCharsets;

public class MainActivity extends AppCompatActivity {

    Button b1,b2;
    EditText e1,e2;
    String s1,s2;
    FileOutputStream f;
    FileInputStream fi;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        b1 = (Button) findViewById(R.id.button);
        b2 = (Button) findViewById(R.id.button2);
        e1 = (EditText) findViewById(R.id.editTextTextPersonName2);
        e2 = (EditText) findViewById(R.id.editTextTextPersonName3);

        b1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                s1 = e1.getText().toString();
                s2 = e2.getText().toString();

                try {
                    f = openFileOutput(s1,MODE_PRIVATE);

                    f.write(s2.getBytes(StandardCharsets.UTF_8));
                    Toast.makeText(MainActivity.this, "saved successfully", Toast.LENGTH_SHORT).show();
                        f.close();
                }catch (Exception e){
                    Toast.makeText(MainActivity.this, e.toString(), Toast.LENGTH_SHORT).show();
                }
            }
        });

        b2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                try {
                    FileInputStream fi = openFileInput(s1);
                    InputStreamReader isr = new InputStreamReader(fi);
                    char[] inp = new  char[20];
                    String s="";
                    int ch;
                    while ((ch = isr.read(inp))>0){
                        String rd=String.copyValueOf(inp,0,ch);
                        s+=rd;
                        inp = new char[20];
                    }
                    Toast.makeText(MainActivity.this,s, Toast.LENGTH_SHORT).show();
                } catch (FileNotFoundException e) {
                    e.printStackTrace();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        });

    }
}